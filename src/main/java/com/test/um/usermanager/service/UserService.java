package com.test.um.usermanager.service;

import com.test.um.usermanager.domain.User;
import com.test.um.usermanager.repo.UserRepository;
import com.test.um.usermanager.web.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    public static final String FRANCE = "France";

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,  PasswordEncoder passwordEncoder){
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Create a new user in the database.
     *
     * @param userDto user information
     * @return Optional of user, empty if the user already exists.
     */
    public Optional<User> createUser(UserDto userDto) {
        LOGGER.info("Creating new user");
        if(getAge(userDto.getDateOfBirth()) <= 18){
            throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Only adults with age > 18 years can sign up.");
        }
        if(!FRANCE.equalsIgnoreCase(userDto.getCountryName())){
            throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Support only for users lived in France");
        }
        Optional<User> user = Optional.empty();
        if (!userRepository.findByUsername(userDto.getUsername()).isPresent()) {
            User newUser = getUser(userDto);
            user = Optional.of(userRepository.save(newUser));
        }
        return user;
    }

    private User getUser(UserDto userDto) {
        User newUser = new User();
        newUser.setUsername(userDto.getUsername());
        newUser.setPassword(userDto.getPassword());
        newUser.setFirstName(userDto.getFirstName());
        newUser.setLastName(userDto.getLastName());
        newUser.setCountryName(userDto.getCountryName());
        newUser.setDateOfBirth(userDto.getDateOfBirth());
        return newUser;
    }

    /**
     * Calculate age of the user until the current date
     * @param birthDate
     * @return user age
     */
    private int getAge(LocalDate birthDate) {
        LocalDate currentDate = LocalDate.now();
        return Period.between(birthDate, currentDate).getYears();
    }

    /**
     * Get user detail information by user name
     * @param username
     * @return Optional of user, empty if the user not found.
     */
    public Optional<User> findUserDetail(String username){
        return userRepository.findByUsername(username);
    }
}

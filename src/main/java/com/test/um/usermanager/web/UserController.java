package com.test.um.usermanager.web;

import com.test.um.usermanager.aspect.Loggable;
import com.test.um.usermanager.domain.User;
import com.test.um.usermanager.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Loggable
    @PostMapping("/signup")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create user")
    public User signup(@RequestBody @Validated UserDto userDto){
        return userService.createUser(userDto).orElseThrow(() -> new HttpServerErrorException(HttpStatus.BAD_REQUEST,"User already exists"));
    }

    @Loggable
    @GetMapping("/{username}")
    @Operation(summary = "Search user detail")
    public User getUserDetail(@PathVariable("username") String username) {
        return userService.findUserDetail(username)
                .orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND, "User " + username + " not found"));
    }
}

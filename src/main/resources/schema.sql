
CREATE TABLE security_user (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  username varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  gender varchar(255) ,
  date_of_birth date NOT NULL,
  country_name varchar(255) NOT NULL,
  city_name varchar(255),
  address varchar(255),
  postal_code varchar(255),
  telephone_number varchar(255),
  created_date DATETIME NOT NULL,
  active Boolean
);
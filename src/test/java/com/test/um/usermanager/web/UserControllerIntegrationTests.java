package com.test.um.usermanager.web;


import com.test.um.usermanager.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpServerErrorException;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerIntegrationTests {

    @Autowired
    UserController userController;

    LocalDate localDate = LocalDate.now().minusYears(20);

    /**
     *  HTTP POST /users/signup
     */
    @Test
    public void testCreateUserHappyPath() {
        UserDto userDto = getUser();
        User user = userController.signup(userDto);
        assertThat(user.getCountryName(), is("France"));
        assertThat(user.getDateOfBirth(), is(localDate));
    }
    /**
     *  HTTP Get /users/{username}
     */
    @Test
    public void testgetUserDetailHappyPath() {
        UserDto userDto = getUser();
        userDto.setUsername("web_interg_testUser2");
        User user = userController.signup(userDto);
        assertThat(user.getCountryName(), is("France"));
        assertThat(user.getDateOfBirth(), is(localDate));

        User searchedUser = userController.getUserDetail("web_interg_testUser2");
        assertThat(searchedUser.getUsername(), is("web_interg_testUser2"));
    }

    /**
     *  HTTP Get /users/{username}
     */
    @Test(expected = HttpServerErrorException.class)
    public void testgetUserDetailUserNotFound() {
        userController.getUserDetail("invalid_user");
    }


    private UserDto getUser() {
        UserDto mockUser = new UserDto();
        mockUser.setUsername("web_interg_testUser");
        mockUser.setPassword("testPassword");
        mockUser.setFirstName("firsName");
        mockUser.setLastName("lastName");
        mockUser.setDateOfBirth(localDate);
        mockUser.setCountryName("France");
        return mockUser;
    }
}

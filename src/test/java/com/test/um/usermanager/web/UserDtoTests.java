package com.test.um.usermanager.web;

import org.junit.Test;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class UserDtoTests {

    @Test
    public void testSetters() {
        LocalDate localDate = LocalDate.now().minusYears(20);
        UserDto dto = new UserDto();
        dto.setUsername("dto_testUser");
        dto.setDateOfBirth(localDate);
        dto.setCountryName("France");
        assertThat(dto.getUsername(), is("dto_testUser"));
        assertThat(dto.getCountryName(), is("France"));
        assertThat(dto.getDateOfBirth(), is(localDate));
    }
}

package com.test.um.usermanager.repo;

import com.test.um.usermanager.domain.User;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    UserRepository userRepository;

    @Test
    public void testFindUserByUserName() {
        User newUser = new User();
        newUser.setUsername("repo_testUser");
        newUser.setPassword("testPassword");
        newUser.setFirstName("firsName");
        newUser.setLastName("lastName");
        newUser.setDateOfBirth(LocalDate.now().minusYears(20));
        newUser.setCountryName("France");
        entityManager.persist(newUser);

        Optional<User> savedUser = userRepository.findByUsername("repo_testUser");
        assertTrue(savedUser.isPresent());
        savedUser = userRepository.findByUsername("unknownUser");
        assertFalse(savedUser.isPresent());
    }
}

package com.test.um.usermanager.service;

import com.test.um.usermanager.domain.User;
import com.test.um.usermanager.repo.UserRepository;
import com.test.um.usermanager.web.UserDto;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.HttpServerErrorException;

import java.time.LocalDate;
import java.util.Optional;


import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class UserServiceTests {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    LocalDate localDate = LocalDate.now().minusYears(20);

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateUserHappyPath() {
        User mockUser = getUser();
        when(this.userRepository.save(any(User.class))).thenReturn(mockUser);

        UserDto userDto = new UserDto();
        userDto.setDateOfBirth(this.localDate);
        userDto.setCountryName("France");

        Optional<User> newUser = this.userService.createUser(userDto);

        // Verify the save
        assertTrue(newUser.isPresent());
        assertEquals("service_testUser", newUser.get().getUsername());
        assertEquals("firsName", newUser.get().getFirstName());
        assertEquals(this.localDate, newUser.get().getDateOfBirth());
    }

    @Test(expected = HttpServerErrorException.class)
    public void testCreateUserInvalidAge() {
        UserDto userDto = new UserDto();
        userDto.setDateOfBirth(LocalDate.now().minusYears(10));
        userDto.setCountryName("France");

        this.userService.createUser(userDto);
    }

    @Test(expected = HttpServerErrorException.class)
    public void testCreateUserInvalidCountryName() {
        UserDto userDto = new UserDto();
        userDto.setDateOfBirth(this.localDate);
        userDto.setCountryName("Japan");

        this.userService.createUser(userDto);
    }

    @Test
    public void findUserDetailHappyPath() {
        User mockUser = getUser();
        mockUser.setUsername("service_testUser2");
        when(this.userRepository.findByUsername("service_testUser2")).thenReturn(Optional.of(mockUser));

        Optional<User> newUser = this.userService.findUserDetail("service_testUser2");

        // Verify the save
        assertTrue(newUser.isPresent());
        assertEquals("service_testUser2", newUser.get().getUsername());
        assertEquals(this.localDate, newUser.get().getDateOfBirth());
    }

    @Test
    public void findUserDetailInvalidUser() {
        Optional<User> newUser = this.userService.findUserDetail("invalid_username");
        // Verify user dos not exist
        assertFalse(newUser.isPresent());
    }

    private User getUser() {
        User mockUser = new User();
        mockUser.setUsername("service_testUser");
        mockUser.setPassword("testPassword");
        mockUser.setFirstName("firsName");
        mockUser.setLastName("lastName");
        mockUser.setDateOfBirth(localDate);
        mockUser.setCountryName("France");
        return mockUser;
    }
}

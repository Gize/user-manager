package com.test.um.usermanager.service;

import com.test.um.usermanager.domain.User;
import com.test.um.usermanager.web.UserDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceIntegrationTests {

    @Autowired
    private UserService userService;

    LocalDate localDate = LocalDate.now().minusYears(20);

    @Test
    public void testCreateUserHappyPath() {
        UserDto userDto = getUser();
        Optional<User> newUser = this.userService.createUser(userDto);
        // Verify the addition
        assertNotNull(newUser.get());
        assertNotNull(newUser.get().getId());
        assertEquals("service_Int_testUser", newUser.get().getUsername());
        assertEquals("firsName", newUser.get().getFirstName());
        assertEquals(this.localDate, newUser.get().getDateOfBirth());
    }

    @Test
    public void testCreateUserAlreadyExist() {
        UserDto userDto = getUser();
        userDto.setUsername("service_Int_testUser2");
        Optional<User> newUser = this.userService.createUser(userDto);
        assertNotNull(newUser.get());
        // try to create same user again
        newUser = this.userService.createUser(userDto);
        // Verify it didn't create second time
        assertFalse(newUser.isPresent());
    }

    private UserDto getUser() {
        UserDto mockUser = new UserDto();
        mockUser.setUsername("service_Int_testUser");
        mockUser.setPassword("testPassword");
        mockUser.setFirstName("firsName");
        mockUser.setLastName("lastName");
        mockUser.setDateOfBirth(localDate);
        mockUser.setCountryName("France");
        return mockUser;
    }
}

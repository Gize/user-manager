package com.test.um.usermanager;

import com.test.um.usermanager.repo.UserRepositoryIntegrationTest;
import com.test.um.usermanager.service.UserServiceIntegrationTests;
import com.test.um.usermanager.web.UserControllerIntegrationTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses ({UserControllerIntegrationTests.class, UserServiceIntegrationTests.class, UserRepositoryIntegrationTest.class})
public class UserManagementTestSuite {
}

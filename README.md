# Getting Started

## UML
![picture](uml.png)

## Environment

[Spring boot](https://spring.io/projects/spring-boot)

[H2 in-memory database ](https://www.h2database.com/html/main.html)

[JDK Oracle 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

[IntelliJ IDEA](https://www.jetbrains.com/idea/)

[Postman](https://www.getpostman.com/)

## Swagger documentation
http://localhost:8080/swagger-ui.html
